package player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;

class BackGround extends JPanel							//class for drawing background elements animating dots
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	double screenWidth = screenSize.getWidth();
	double screenHeight = screenSize.getHeight();

	private Random randomCoordinate = new Random();						//random number generator for initial coordinates;
	private int numberOfDots = (int) (screenWidth * screenHeight) / 11000 ;
	
	int[] x = new int[numberOfDots];
	int[] y = new int[numberOfDots];
	boolean[] flagX = new boolean[numberOfDots];
	boolean[] flagY = new boolean[numberOfDots];
	
	public BackGround()
	{
		for(int i = 0; i<numberOfDots; i++)
		{
			x[i] = 10 + randomCoordinate.nextInt((int)screenWidth - 10);
			y[i] = 10 + randomCoordinate.nextInt((int)screenHeight - 10);
		}	
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		
		Graphics2D g2 = (Graphics2D) g;								//
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,		//ANTIALIASED PANEL!!!!!
         RenderingHints.VALUE_ANTIALIAS_ON);						//
        
		super.paintComponent(g2);
		super.paintComponent(g);
		
		for(int j = 0; j<numberOfDots; j++)				//draw circles
		{
			g.setColor(new Color(204,229,255));
			g.fillOval(x[j], y[j], 4, 4);	
		}
		
		for(int a = 0; a<numberOfDots; a++)					//draw lines
		{
			for(int b=0; b<numberOfDots; b++)
			{
				if((Math.abs(x[a] - x[b]) < 50) && (Math.abs(y[a]-y[b]) < 50))
				{
						g.setColor(new Color(204,229,255, 255));
						g.drawLine(x[a], y[a], x[b], y[b]);		
				}
				if((Math.abs(x[a] - x[b]) < 60) && (Math.abs(y[a]-y[b]) < 60))
				{
						g.setColor(new Color(204,229,255, 200));
						g.drawLine(x[a], y[a], x[b], y[b]);		
				}
				if((Math.abs(x[a] - x[b]) < 70) && (Math.abs(y[a]-y[b]) < 70))
				{
						g.setColor(new Color(204,229,255, 150));
						g.drawLine(x[a], y[a], x[b], y[b]);		
				}
				if((Math.abs(x[a] - x[b]) < 80) && (Math.abs(y[a]-y[b]) < 80))
				{
						g.setColor(new Color(204,229,255, 100));
						g.drawLine(x[a], y[a], x[b], y[b]);		
				}
				if((Math.abs(x[a] - x[b]) < 90) && (Math.abs(y[a]-y[b]) < 90))
				{
						g.setColor(new Color(204,229,255, 50));
						g.drawLine(x[a], y[a], x[b], y[b]);		
				}
				if((Math.abs(x[a] - x[b]) < 100) && (Math.abs(y[a]-y[b]) < 100))
				{
						g.setColor(new Color(204,229,255, 20));
						g.drawLine(x[a], y[a], x[b], y[b]);		
				}
			}
		}
   }

	ActionListener update = new ActionListener() {
	@Override
	public void actionPerformed(ActionEvent arg0)
		{
		// TODO Auto-generated method stub
		for(int k = 0; k < numberOfDots; k++)
		{
			if(x[k] == screenWidth)
				flagX[k] = true;
			else if(x[k] == 0)
				flagX[k] = false;
			if(y[k] == screenHeight)
				flagY[k] = true;
			else if(y[k] == 0)
				flagY[k] = false;
			
			//motion algorithm 
			if(!flagX[k] && !flagY[k])
			{
				if(k%2 == 0)
					{
					x[k] += 0.9;
					y[k] += 1.1;
					}
				else if(k%3 == 0)
					{
					x[k] += 1.7;
					y[k] += 1.8;
					}
				else if(k%5 == 0)
				{
					x[k] += 1.4;
					y[k] += 2.0;
				}
				else 
				{
					x[k] += 1.1;
					y[k] += 0.9;
				}	
				
			}

			else if(flagX[k] && flagY[k])
			{
				if(k%2 == 0)
				{
				x[k] -= 0.9;
				y[k] -= 1.1;
				}
			else if(k%3 == 0)
				{
				x[k] -= 1.7;
				y[k] -= 1.8;
				}
			else if(k%5 == 0)
			{
				x[k] -= 1.4;
				y[k] -= 2.0;
			}
			else 
			{
				x[k] -= 1.1;
				y[k] -= 0.9;
			}	
					
			}

			else if(flagX[k] && !flagY[k])
			{
				if(k%2 == 0)
				{
				x[k] -= 0.9;
				y[k] += 1.1;
				}
			else if(k%3 == 0)
				{
				x[k] -= 1.7;
				y[k] += 1.8;
				}
			else if(k%5 == 0)
			{
				x[k] -= 1.4;
				y[k] += 2.0;
			}
			else 
			{
				x[k] -= 1.1;
				y[k] += 0.9;
			}	
					
			}

			else if(!flagX[k] && flagY[k])
			{
				if(k%2 == 0)
				{
				x[k] += 0.9;
				y[k] -= 1.1;
				}
			else if(k%3 == 0)
				{
				x[k] += 1.7;
				y[k] -= 1.8;
				}
			else if(k%5 == 0)
			{
				x[k] += 1.4;
				y[k] -= 2.0;
			}
			else 
			{
				x[k] += 1.1;
				y[k] -= 0.9;
			}	
					
			}
			//end motion algorithm
			
		}
			repaint();
	
	}
	};
	
	public void addGlass(JPanel glass)
	{
		
		//this.setLayout(new BorderLayout());
		glass.setBackground(new Color(40,40,50,130));
		//glass.setSize((int)MusicPlayer.masterSize.getWidth(), (int)MusicPlayer.masterSize.getHeight());
		//glass.setPreferredSize(glass.getSize());
		this.add(glass, BorderLayout.CENTER);
	}	
}