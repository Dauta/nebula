package player;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import javazoom.jl.player.Player;
import javazoom.jl.decoder.Equalizer;

public class Music
{
	
	 String bufferedSong; 	
	 String fileName;
	 int songCounter = 0;
	 Player mp3player;
	 MenuBar menu;
	 Playlist playlist;
	 boolean nowPlaying = false;
	 boolean nowPaused = false;
	 boolean finished = true;
	 JButton playButton = new JButton("Play");
	 JButton stopButton = new JButton("Stop");
	 JButton nextButton = new JButton("Next");
	 JButton prevButton = new JButton("Previous");
	 public BufferedInputStream buffStream;
	 public FileInputStream stream;
	 long audioLength = 0;
	 long audioPaused = 0;
	 Thread musicThread;  
	 String song;
	// String currentSong;
	 
	 public Music(MenuBar mb, Playlist pl)
	 {
		 		menu = mb;
		 	    playlist = pl;
	 }
	
	void LoadStream()
	{
		
		try
		{	
			song = ClickableLabel.songName;
			fileName = (String)playlist.mainList.get(Playlist.name).get(song);
			
			
			stream = new FileInputStream(fileName);
			audioLength = stream.available();				//get the length of a full file in bytes;
			buffStream = new BufferedInputStream(stream);			
			mp3player = new Player(buffStream);		
			
		}
		catch (Exception e)
		{
				System.out.println(e);
				e.printStackTrace();
		}
		
		
	}
	
	ActionListener PlayMp3 = new ActionListener()
	{
		@Override
	public void actionPerformed(ActionEvent arg0)
	{
			if(song == null)
			{
				playlist.OpenSingleFile();
			}
			else
			{
			Play();
			}
	}
			
	};			//end PlayMp3 ActionListener
	
	ActionListener StopMp3 = new ActionListener()
	{
		public void actionPerformed(ActionEvent args0)
		{
			Stop();
		}
				
	};
	
	ActionListener PauseMp3 = new ActionListener()
	{
		public void actionPerformed(ActionEvent args0)
		{
			Pause();
		}	
	};
	
	ActionListener durationListener = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent args0)
		{
				CheckDuration();
		}
	};
	
	ActionListener changeListener = new ActionListener()				//check this!!!!!
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if(song != ClickableLabel.songName)
			{
					Stop();
					Play();
			}
		}
	};
	
	ActionListener nextBtnListener = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Stop();	
			
			try
			{
			//automaticly go to the next one;
				
				System.out.println("asd");
			Iterator<Entry<String, String>> it = playlist.mainList.get(Playlist.name).entrySet().iterator();
			while(it.hasNext())
			{	
				
			//	System.out.println(ClickableLabel.songName + "whatever");
				Entry<String, String> curr = it.next();
				//System.out.println(curr.getKey());
				if(curr.getKey().equals(song))
				{
					ClickableLabel.songName = it.next().getKey();
					System.out.println(ClickableLabel.songName);
					break;
				}
			}
			}
			catch(Exception asd)
			{
				System.out.println("no more");
			}
		}
	};
	
	ActionListener prevBtnListener = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{			
			try
			{
			//automaticly go to the previous one
				Stop();	
			Iterator<Entry<String, String>> it = playlist.mainList.get(Playlist.name).entrySet().iterator();
			while(it.hasNext())
			{	
				String[] curr = new String[100];
				
				for(int i = 0; i<playlist.mainList.get(Playlist.name).size(); i++)
				{
				 curr[i] = it.next().getKey();
				if(curr[i].equals(song))
				{
					if(i > 0)
					{
					ClickableLabel.songName = curr[i-1];
					break;
					}
				}
				}
			}
			}
			catch(Exception ex)
			{
				Play();
			}
		}
		
	};
	
	void Play()
	{
		// LoadStream();
		
		musicThread = new Thread() {
	        public void run() {
	            try 
	            {
	        //    	System.out.println("New Thread");
	            //	System.out.println(ClickableLabel.songName);
	            	mp3player.play();
	            	
	            	
	            	
	            	
	         //   	System.out.println("Thread finished");
	            }
	            catch (Exception e) 
	            { 
	            	System.out.println(e); 
	            	e.printStackTrace();
	            }
	        }
	    };
	    
	    
	 //  System.out.println( musicThread.getUncaughtExceptionHandler());
		
	//    song = ClickableLabel.songName;
		if(nowPlaying == false && nowPaused == false && finished == true)
		{
			//get a file path and set up streams and player
			 LoadStream();
			 	
			musicThread.start();					//start thread to play music
			nowPlaying = true;						//nowPlaying flag set to true
			finished = false;
		}
		
		else if(nowPaused == true)			
		{
			 try													//get a file path and set up streams and player
				{	
				LoadStream();
				 	
				// ClickableLabel.songName = currentSong;
				// System.out.println(audioLength);
				buffStream.skip(audioLength - audioPaused - 25000);
					musicThread.start();
					audioPaused = 0;						//reset the byte counter 
					nowPaused = false;						//reset the nowPaused flag
					nowPlaying = true;						//set the nowPlaying flag
							
				}
					catch (Exception e)
				{
						System.out.println(e);
						e.printStackTrace();
				}

		}
		
		else
		{
			System.out.println("Already Playing can't start another");
		}
	
		ChangeButton();
		
	}       // end Play()
	
	
	
	public void Stop()
	{
		if(nowPlaying == true)
		{
		try
		{
	//		System.out.println("stopped");
			mp3player.close();
			nowPlaying = false;
			nowPaused = false;
			audioPaused = 0;				//reset the byte counter;	
			ChangeButton();
			finished = true;
		//	System.out.println("stop finished");
			musicThread.interrupt();
		}
		catch (Exception e)
		{
			System.out.println("nothing to stop");
			e.printStackTrace();
		}
		
		}
		
		else if(nowPaused == true)
		{
			audioPaused = 0;
			nowPaused = false;
			nowPlaying = false;
			finished = true;
		}
		else
		{
			System.out.println("can't stop");
		}
	}				
	//end Stop();

	public void Pause()
	{
		System.out.println("Paused");
		try {
			this.audioPaused = stream.available();		
	//		System.out.println(audioPaused);
		}
			catch (Exception e) 
		{
			e.printStackTrace();
		}	
		
		musicThread.interrupt();
		mp3player.close();
		this.nowPlaying = false;		
		this.nowPaused = true;
		this.finished = false;
		ChangeButton();		
		
	}		
	//end Pause();
	
	void ChangeButton()
	{
		if (nowPlaying == true)
		{
			playButton.removeActionListener(PlayMp3);
			playButton.addActionListener(PauseMp3);
			//playButton.setText("Pause");
			playButton.setIcon(new ImageIcon("src\\Image\\pauseBtn2.png"));
			//System.out.println("1");
		}
		else if (nowPlaying == false)
		{
			playButton.removeActionListener(PauseMp3);
			playButton.addActionListener(PlayMp3);
			playButton.setIcon(new ImageIcon("src\\Image\\playBtn2.png"));
		//	System.out.println("2");
		}
		else if(finished == true)
		{
			playButton.removeActionListener(PauseMp3);
			playButton.addActionListener(PlayMp3);
			playButton.setIcon(new ImageIcon("src\\Image\\playBtn2.png"));
			//System.out.println("2");
			audioPaused = 0;
		}
	}
	//end ChangeButton
	
	
	public void CheckDuration()						//works <3
	{
		
		if(nowPlaying)
		{
			try{
				if(!musicThread.isAlive())
				{
					Stop();	
					//automaticly go to the next one;
					
					@SuppressWarnings("unchecked")
					Iterator<Entry<String, String>> it = playlist.mainList.get(Playlist.name).entrySet().iterator();
					while(it.hasNext())
					{
						Entry<String, String> curr = it.next();
					
						if(curr.getKey().equals(song))
						{	
							ClickableLabel.songName = it.next().getKey();
							break;
						}
					}
					
				}
			}
			catch(Exception e)
			{
						System.out.println("damn");
			}
		}
	
	}
	
	
	
	
}  