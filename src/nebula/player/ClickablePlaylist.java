package player;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ClickablePlaylist extends JPanel implements MouseListener
{

	JPanel panel;
	JPanel bigPanel;
	Playlist list;
	JTextField inputField;
	JLabel nameText;
	String newName = "";
	
	static boolean flag;
	
	public ClickablePlaylist(JPanel p, JPanel p2, Playlist pl, String x)
	{
		newName = x;
		panel = p;
		bigPanel = p2;
		list = pl;
		panel.setLayout(new FlowLayout());
		nameText = new JLabel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		this.addMouseListener(this);
		
		if(!flag)
		{
			this.setBackground(new Color(200, 210, 250, 70));
		}
		else if(flag)
		{
			this.setBackground(new Color(250,250,250,120));
		}
		
		flag = !flag;
	}
	
	public ClickablePlaylist(JPanel p, JPanel p2, Playlist pl)
	{
		panel = p;
		bigPanel = p2;
		list = pl;
		
		panel.setLayout(new FlowLayout());
		inputField = new JTextField("");
		nameText = new JLabel();
		
		inputField.setSize(panel.getWidth(), panel.getHeight() * 5/100);
		inputField.setPreferredSize(inputField.getSize());
		inputField.setMaximumSize(inputField.getSize());
		inputField.addKeyListener(this.enterListener);
		//panel.add(inputField);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		
            	panel.add(inputField);
                panel.revalidate();
               panel.repaint();
   
		
		
	//	this.setPreferredSize(this.getSize());
	//	this.setMaximumSize(this.getSize());
		//this.setOpaque(false);
	//	
		
		
		this.addMouseListener(this);
		
		if(!flag)
		{
			this.setBackground(new Color(200, 210, 250, 70));
		}
		else if(flag)
		{
			this.setBackground(new Color(250,250,250,120));
		}
		
		flag = !flag;
	
	}
	
	
	public void insertPlaylist()
	{
		
		if(inputField.getText().length() != 0)
		{
			System.out.println("enter");
			panel.remove(inputField);
			
			nameText.setText(inputField.getText());
			nameText.setFont(new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 15));
			nameText.setForeground(new Color(240,240,255));
			this.setSize(panel.getWidth(), panel.getHeight() * 5/100);
			this.setPreferredSize(this.getSize());
			this.setMaximumSize(this.getSize());
			this.add(nameText);
		
	            	panel.add(this);
	    			panel.revalidate();
	    			panel.repaint();
	         
			
			if(Playlist.name == null)
			{
				Playlist.name = nameText.getText();
			}
			
			list.mainList.put(nameText.getText(), list.fileList);
			list.fileList.clear();
		}
	}
	
	public void autoInsertPlaylist()
	{
		
		nameText.setText(newName);
		nameText.setFont(new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 15));
		nameText.setForeground(new Color(240,240,255));
		this.setSize(panel.getWidth(), panel.getHeight() * 5/100);
		this.setPreferredSize(this.getSize());
		this.setMaximumSize(this.getSize());
		this.add(nameText);
	
            	panel.add(this);
    			panel.revalidate();
    			panel.repaint();
         
		
		if(Playlist.name == null)
		{
			Playlist.name = nameText.getText();
		}
		
		//list.mainList.put(nameText.getText(), list.fileList);
	//	list.fileList.clear();
	}
	
	
	
	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
	
@Override
public void mouseClicked(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseEntered(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void mousePressed(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	if (e.getClickCount() % 2 == 0 && !e.isConsumed()) 
	{
	     e.consume();
	     
	     Playlist.name = this.nameText.getText();
	     System.out.println(Playlist.name);
	 
	     System.out.println(list.mainList.get(Playlist.name));
	     if(!list.mainList.get(Playlist.name).isEmpty())
	     {
	    	 System.out.println("inside the if");
	    	 bigPanel.removeAll();
	    	 
	    	// for(int i = 0; i < list.mainList.get(Playlist.name).size(); i++)
	    	// {
	    		 
	    		 Iterator<Entry<String, String>> it = list.mainList.get(Playlist.name).entrySet().iterator();
					while(it.hasNext())
					{
						Entry<String, String> curr = it.next();
						
						String[] temp = curr.getKey().split(" - ");
						bigPanel.add(new ClickableLabel(temp[1], temp[0], list));
						System.out.println(temp[1] + temp[0]);
						
					}

	     }
	     else
	     {
	    	 System.out.println("inside the else");
	    	 bigPanel.removeAll();
	     }
	     bigPanel.revalidate();
	     bigPanel.repaint();
	}
}

  public KeyListener enterListener = new KeyListener()
  {

	@Override
	public void keyPressed(KeyEvent arg0) 
	{
		
		 Playlist.name = inputField.getText();
		
		if(arg0.getKeyCode() == KeyEvent.VK_ENTER)
		{
			if(!list.mainList.containsKey(inputField.getText()))
			{
			insertPlaylist();
			list.createNewTable(Playlist.name, list.fileList);
			
			System.out.println(list.mainList);
			}
			else
			{
				infoBox("Playlist with this name already exists. ", "Oops!");
			}
			
		}
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	  
  };

}
