package player;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.blinkenlights.jid3.*;

public class Playlist {

    public static String name;

    LinkedHashMap<String, LinkedHashMap> mainList = new LinkedHashMap<String, LinkedHashMap>();
    public LinkedHashMap<String, String> fileList = new LinkedHashMap<String, String>();
    public List<String> playlist = new ArrayList<String>();

    public File selectedFile;
    public File folderName;
    JPanel playlistDisplay;

    public String listName;

    public Playlist(JPanel pld) {
        playlistDisplay = pld;
    }


    public void scanFolder(File[] folder)
    {
        String name = "";
        String artist = "";
        String album = "";
        String fullName = "";

        for (File song : folder) {
            if (song.isFile() && song.getName().toLowerCase().endsWith(".mp3")) {

                MediaFile songFile = new MP3File(song);
                try {
                    name = songFile.getID3V1Tag().getTitle().trim();
                } catch (Exception e) {
                    name = song.getName();
                }

                try {
                    artist = songFile.getID3V1Tag().getArtist().trim();
                } catch (Exception e) {
                    artist = "";
                }

                try {
                    album = songFile.getID3V1Tag().getAlbum().trim();
                } catch (Exception e) {
                    album = "";
                }

                fullName = name + " - " + artist;                // getFullname


                if (name == "" || name == " " || name == null || name.length() == 0 ||
                        album == "" || album == " " || album == null || album.length() == 0 ||
                        artist == "" || artist == " " || artist == null || artist.length() == 0) {

                    playlist.add(song.getName());
                    fileList.put(song.getName() + " - " + "", song.getAbsolutePath());

                    ClickableLabel cl;

                    cl = new ClickableLabel("", song.getName(), this);

                    playlistDisplay.add(cl);
                    cl.addMouseListener(cl);


                    playlistDisplay.revalidate();
                    playlist.clear();

                } else {
                    playlist.add(fullName);
                    fileList.put(fullName, song.getAbsolutePath());

                    ////////////////////////////////////////addtracks();
                    ClickableLabel cl;

                    cl = new ClickableLabel(artist, name, this);
                    playlistDisplay.add(cl);
                    //cl.addMouseListener(cl);

                    playlistDisplay.revalidate();
                    playlist.clear();


                }
            }

            else if(song.isDirectory())
            {
                scanFolder(song.listFiles());
            }


        }

    }

    public void GetList() {

        JFileChooser folderScanner = new JFileChooser(new File("D:\\Music"));
        folderScanner.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        FileFilter mp3Filter = new FileNameExtensionFilter("MP3 File", "mp3");
        folderScanner.setFileFilter(mp3Filter);


        // start here
        int result = folderScanner.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            folderName = folderScanner.getSelectedFile();
            //System.out.println(selectedFile.getAbsolutePath());

        }



        File[] folder = new File(folderName.getAbsolutePath()).listFiles();

        scanFolder(folder);

        //end here

        //	if(mainList.containsKey(Playlist.name))
        //	{
        LinkedHashMap<String, String> temp = new LinkedHashMap<String, String>();

        temp.putAll(mainList.get(Playlist.name));

        LinkedHashMap newFiles = fileList;

        Iterator<Entry<String, String>> it = newFiles.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, String> curr = it.next();
            temp.put(curr.getKey(), curr.getValue());
        }

        mainList.remove(Playlist.name);

        mainList.put(Playlist.name, temp);

        System.out.println(mainList.get(Playlist.name));

        fileList.clear();
        //temp.clear();
        newFiles.clear();

        System.out.println(Playlist.name);
    }


    public void OpenSingleFile() {
        JFileChooser fileChooser;
        fileChooser = new JFileChooser(new File("E:\\Music"));
        fileChooser.setDialogTitle("Open File...");

        FileFilter mp3Filter = new FileNameExtensionFilter("MP3 File", "mp3");
        fileChooser.setFileFilter(mp3Filter);

        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
            //System.out.println(selectedFile.getAbsolutePath());

            MediaFile songFile = new MP3File(selectedFile);
            String name = selectedFile.getName();
            String artist = "";
            String album = "";

            try {
                name = songFile.getID3V1Tag().getTitle().toString().trim();
                artist = songFile.getID3V1Tag().getArtist().toString().trim();
                album = songFile.getID3V1Tag().getAlbum().toString().trim();
                //name = tags[0].toString();
            } catch (Exception e) {
                System.out.println("no tags ;(");
            }

            String fullName = name + " - " + artist;

            ClickableLabel cl2;

            if (name.length() == 0 || artist.length() == 0) {
                //fileList.put(selectedFile.getName() + " - " + "", selectedFile.getAbsolutePath());

                mainList.get(Playlist.name).put(selectedFile.getName() + " - " + "", selectedFile.getAbsolutePath());
                cl2 = new ClickableLabel("", selectedFile.getName(), this);
            } else {
                //	fileList.put(fullName, selectedFile.getAbsolutePath());

                mainList.get(Playlist.name).put(fullName, selectedFile.getAbsolutePath());
                cl2 = new ClickableLabel(artist, name, this);
            }

            playlistDisplay.add(cl2);

            //  cl2.addMouseListener(cl2);
            ClickableLabel.songName = fullName;
            System.out.println(fullName);


            //singles.add(selectedFile.getAbsolutePath());
            // playlist.clear();


        }

    }

    void createNewTable(String name, LinkedHashMap<String, String> list) {
        String tableName = name;

        mainList.put(name, list);
    }

}