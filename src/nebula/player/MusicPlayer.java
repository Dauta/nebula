package player;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

public class MusicPlayer {
	

	static Dimension masterSize;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Start Here");
		
		JFrame mainFrame;
		
		Glass glass;
		Seekbar seek;
		Music music;
		Playlist pl;
		MenuBar mainMenu;
		JPanel display;
		JPanel displayPlaylists;
		JPanel storeDisplays;
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double screenWidth = screenSize.getWidth();
		double screenHeight = screenSize.getHeight();
		
		mainFrame = new JFrame();
		mainFrame.setSize((int)screenWidth * 80/100 , (int)screenHeight * 82/100);	
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(new BorderLayout());
		mainFrame.setResizable(false);
		//mainFrame.setUndecorated(true);			     <-------------------------- think about this!!!!
		masterSize = mainFrame.getSize();
		
		display  = new JPanel();
		display.setBackground(new Color(75,100,105,50));
		//display.setOpaque(false);
		display.setLayout(new BoxLayout(display, BoxLayout.Y_AXIS));
		
		displayPlaylists = new JPanel();
		displayPlaylists.setBackground(new Color(50,80,85,50));
		//displayPlaylists.setOpaque(false);
		displayPlaylists.setLayout(new BoxLayout(displayPlaylists, BoxLayout.Y_AXIS));
		
		
		
		pl = new Playlist(display);
		mainMenu = new MenuBar(pl, displayPlaylists, display);
		mainMenu.setBackground(new Color(100,150,200));
		mainMenu.ChoosePlaylistFolder();
	
		
		
		BackGround bottomLayer = new BackGround();
		music = new Music(mainMenu, pl);							//check this later :*
		glass = new Glass(music);
		//glass.setSize(masterSize);							//revert if broken
		//glass.setPreferredSize(glass.getSize());
		glass.setLayout(new FlowLayout());
		seek = new Seekbar(music);
		seek.addMouseListener(seek);
		seek.setBackground(new Color(80,110,120,110));
		bottomLayer.setBackground(new Color(65,102,102));		//background color
		
		bottomLayer.addGlass(glass);								//	<--  fixed for now
		mainFrame.add(bottomLayer);
		seek.setSize((int)masterSize.getWidth() * 98/100, (int)masterSize.getHeight() * 5/100);					//revert if broken
		seek.setPreferredSize(seek.getSize());
	
		JScrollPane jspLeft = new JScrollPane(displayPlaylists);
		jspLeft.setSize(masterSize.width *25/100, masterSize.height * 75/100);
		jspLeft.setPreferredSize(jspLeft.getSize());
		jspLeft.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	//	jspLeft.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jspLeft.setBackground(new Color(0,0,0,0));
		
		
		JScrollPane jsp = new JScrollPane(display);
		jsp.setSize(masterSize.width * 73/100, masterSize.height * 75/100);
		jsp.setPreferredSize(jsp.getSize());
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jsp.setBackground(new Color(0,0,0,0));
		
		glass.setSize((int)MusicPlayer.masterSize.getWidth(), (int)MusicPlayer.masterSize.getHeight());
		glass.setPreferredSize(glass.getSize());
		glass.add(jspLeft);
		glass.add(jsp);
		
		new Timer (30, glass).start();
		new Timer (30, bottomLayer.update).start();			//animation timer
		new Timer (30, seek.sliderListener).start();		//seek bar timer
		new Timer (30, music.durationListener).start();     //check for remaining time
		new Timer (30, music.changeListener).start();
		
		glass.add(seek);
		mainFrame.setJMenuBar(mainMenu);
		
		mainFrame.pack();
		mainFrame.setVisible(true);
		
		mainMenu.LoadPlaylist();
		
	}
	
	
}





	
	