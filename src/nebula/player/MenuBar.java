package player;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

@SuppressWarnings("serial")
public class MenuBar extends JMenuBar
{

	Playlist list;
	JMenu file, playlist;
	JMenuItem open, importFolder, createNewPlaylist, save, load;
	JFileChooser fileChooser;
	public File selectedFile;
	public ClickablePlaylist cp;
	JPanel panel, bigPanel;
	
	public MenuBar(Playlist pl, JPanel p, JPanel p2)
	{
		
		panel = p;
		bigPanel = p2;
		this.setBackground(new Color(100,200,200,20));
		
		file = new JMenu("File");
		file = new JMenu("Folder");
		playlist = new JMenu("Playlist");
		
		open = new JMenuItem("Open File...");
		importFolder = new JMenuItem("Import a Folder...");
		createNewPlaylist = new JMenuItem("Create new Playlist...");
		save = new JMenuItem("Save Playlists...");
		load = new JMenuItem("Load Playlist...");
		
		open.addActionListener(openFileListener);
		createNewPlaylist.addActionListener(createNewList);
		save.addActionListener(saveListener);
		load.addActionListener(loadListener);
		playlist.add(createNewPlaylist);
		playlist.add(save);
		playlist.add(load);
		file.add(open);
		file.add(importFolder);
	
		this.add(playlist);
		this.add(file);		
		
		this.setVisible(true);
		list = pl;
		
	}

	public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
	
	ActionListener openFileListener = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			
			if(!(Playlist.name == null))
			list.OpenSingleFile();
			else
			{
				infoBox("Create a playlist first", "Oops");
				cp = new ClickablePlaylist(panel, bigPanel, list);	
				cp.insertPlaylist();
			}
		}
		
	};
	
	ActionListener playlistListener = new ActionListener()
	{
		public void actionPerformed(ActionEvent arg0) {
	
			if(Playlist.name != null)
			{
				list.GetList();
			}
			else
			{
				infoBox("Create a playlist first", "Oops");
				cp = new ClickablePlaylist(panel, bigPanel, list);	
				cp.insertPlaylist();
			}
			
			
		}
	};
	
	ActionListener saveListener = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			savePlaylist();
			
		}
		
	};
	
	ActionListener createNewList = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e) 
		{  
			cp = new ClickablePlaylist(panel, bigPanel, list);	
			cp.insertPlaylist();	
		}
		
	};
	
	ActionListener loadListener = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent arg0) 
		{
			LoadPlaylist();
		}
		
	};
	
	public void ChoosePlaylistFolder()
	{
		importFolder.addActionListener(playlistListener);
	}
	
	void savePlaylist()
	{
		String userName = System.getProperty("user.name");
		String savePath = "C:\\Users\\"+userName+"\\Documents\\TheMusicPlayer";
		
		File f = new File(savePath);
		
		if(f.exists() && f.isDirectory())
		{
			File old = new File(savePath + "\\playlist.txt");
			old.delete();
		
		try{
			  FileWriter fstream = new FileWriter(savePath + "\\playlist.txt");
			  BufferedWriter out = new BufferedWriter(fstream);
			 
			Iterator<Entry<String, LinkedHashMap>> it = list.mainList.entrySet().iterator();
			while(it.hasNext())
			{
				Entry<String, LinkedHashMap> currentList = it.next();
				LinkedHashMap<String, String> currentMap = currentList.getValue();
		
				out.write(currentList.getKey()+"FFFF");
				out.newLine();
				
				
				Iterator<Entry<String, String>> it2 = currentMap.entrySet().iterator();
				while(it2.hasNext())
				{
				Entry<String, String> thePlaylist = it2.next();
				
				out.write(thePlaylist.getKey()+"SSSPLITTERRR"+thePlaylist.getValue());
				out.newLine();
				}
			}
			  
			  
			  
			  
			  
			  
			  //out.write("Hello Java");
			  //Close the output stream
			  out.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			f.mkdir();
			
			try{
				  FileWriter fstream = new FileWriter(f.getAbsolutePath()+"\\playlist.txt");
				  BufferedWriter out = new BufferedWriter(fstream);
				 
				Iterator<Entry<String, LinkedHashMap>> it = list.mainList.entrySet().iterator();
				while(it.hasNext())
				{
					Entry<String, LinkedHashMap> currentList = it.next();
					LinkedHashMap<String, String> currentMap = currentList.getValue();
			
					out.write(currentList.getKey());
					out.newLine();
					
					Iterator<Entry<String, String>> it2 = currentMap.entrySet().iterator();
					while(it2.hasNext())
					{
					Entry<String, String> thePlaylist = it2.next();
					
					out.write(thePlaylist.getKey()+" "+thePlaylist.getValue());
					out.newLine();
					}
				}
				  out.close();
		}
			catch(Exception e)
			{
				e.printStackTrace();
			}	
		}
	}
	
	void LoadPlaylist()
	{

		String userName = System.getProperty("user.name");
		String loadPath = "C:\\Users\\"+userName+"\\Documents\\TheMusicPlayer\\playlist.txt";
		BufferedReader br;
		File f = new File(loadPath);
		String tempOuterKey = null;
		String tempInnerKey = null;
		String tempInnerValue = null;
		LinkedHashMap<String, String> tempMap = new LinkedHashMap<String,String>();
		String currentLine; 
		String toBeName = null;
		
		if(f.exists())
		{
			
			try
			{
				br = new BufferedReader(new FileReader(loadPath));
		
				while ((currentLine = br.readLine()) != null) 
				{
				//	System.out.println(currentLine);
					
					if(currentLine.substring(currentLine.length() - 4).equals("FFFF"))
					{
						tempOuterKey = currentLine.substring(0, currentLine.length() - 4);
						toBeName = tempOuterKey;
						//System.out.println(tempOuterKey+" tempOuterKey");
						tempMap.clear();
					}
					else
					{
						String[] splitString = currentLine.split("SSSPLITTERRR");
						tempInnerKey = splitString[0];
						System.out.println(tempInnerKey);
					    tempInnerValue = splitString[1];
						System.out.println(tempInnerValue);
						//tempMap = new LinkedHashMap<String,String>();
						tempMap.put(tempInnerKey, tempInnerValue);
					}
					
					//System.out.println(tempMap);
					LinkedHashMap<String, String> asd = new LinkedHashMap<String, String>();
					
					asd.putAll(tempMap);
					list.mainList.put(tempOuterKey, asd);
					//tempMap.clear();
					
				}
				System.out.println(list.mainList);
			//	playlist.name = list.mainList.
				//System.out.println("asd");
				
				br.close();
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		
		}
		
		
		System.out.println(list.mainList);
		Iterator<String> it = list.mainList.keySet().iterator();
		while(it.hasNext())
		{
			String curr = it.next();
			
			System.out.println(curr);
			
			ClickablePlaylist playlistToCreate = new ClickablePlaylist(panel, bigPanel, list, curr);
			playlistToCreate.autoInsertPlaylist();
		}
		
		Playlist.name = toBeName;
		System.out.println(list.mainList);
	}
}
