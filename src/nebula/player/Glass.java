package player;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Glass extends JPanel implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Music music;
	String ttl = "     ";
	JLabel title;
	public Glass(Music m)
	{
		
		new Timer(30, this).start();
		
		this.setLayout(new BorderLayout());
		music = m;
		music.playButton.addActionListener(music.PlayMp3);				//or play WAV
		music.stopButton.addActionListener(music.StopMp3);
		music.nextButton.addActionListener(music.nextBtnListener);
		music.prevButton.addActionListener(music.prevBtnListener);
	
		ImageIcon playIcon = new ImageIcon("src\\Image\\playBtn2.png");
		//URL url = getClass().getResource("src/Image/PlayBtn2.png");
		//ImageIcon playIcon = new ImageIcon(url);
		music.playButton.setBorder(BorderFactory.createEmptyBorder());
		music.playButton.setContentAreaFilled(false);
		music.playButton.setFocusable(false);
		music.playButton.setIcon(playIcon);
		music.playButton.setSize((int)MusicPlayer.masterSize.getWidth() * 5/100, (int)MusicPlayer.masterSize.getWidth() * 5/100);
		music.playButton.setPreferredSize(music.playButton.getSize());
		
		ImageIcon stopIcon = new ImageIcon("src\\Image\\stopBtn2.png");
		music.stopButton.setBorder(BorderFactory.createEmptyBorder());
		music.stopButton.setContentAreaFilled(false);
		music.stopButton.setFocusable(false);
		music.stopButton.setIcon(stopIcon);
		music.stopButton.setSize((int)MusicPlayer.masterSize.getWidth() * 5/100, (int)MusicPlayer.masterSize.getWidth() * 5/100);
		music.stopButton.setPreferredSize(music.stopButton.getSize());
	
		ImageIcon nextIcon = new ImageIcon("src\\Image\\nextBtn.png");
		music.nextButton.setBorder(BorderFactory.createEmptyBorder());
		music.nextButton.setContentAreaFilled(false);
		music.nextButton.setFocusable(false);
		music.nextButton.setIcon(nextIcon);
		music.nextButton.setSize((int)MusicPlayer.masterSize.getWidth() * 5/100, (int)MusicPlayer.masterSize.getWidth() * 5/100);
		music.nextButton.setPreferredSize(music.stopButton.getSize());
		
		ImageIcon prevIcon = new ImageIcon("src\\Image\\prevBtn.png");
		music.prevButton.setBorder(BorderFactory.createEmptyBorder());
		music.prevButton.setContentAreaFilled(false);
		music.prevButton.setFocusable(false);
		music.prevButton.setIcon(prevIcon);
		music.prevButton.setSize((int)MusicPlayer.masterSize.getWidth() * 5/100, (int)MusicPlayer.masterSize.getWidth() * 5/100);
		music.prevButton.setPreferredSize(music.stopButton.getSize());
	
		JPanel buttonPanel = new JPanel();
		buttonPanel.setSize((int)MusicPlayer.masterSize.getWidth() * 95/100, (int)MusicPlayer.masterSize.getHeight() * 10/100 );
		buttonPanel.setPreferredSize(buttonPanel.getSize());
		buttonPanel.setBackground(new Color(0,0,0,0));
		
		buttonPanel.add(music.prevButton);
		buttonPanel.add(music.playButton);	
		buttonPanel.add(music.stopButton);	
		buttonPanel.add(music.nextButton);
		
		this.add(buttonPanel, BorderLayout.NORTH);
	
	}	
		
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(ttl != ClickableLabel.songName)
		{
			try
			{
			this.remove(title);
			}
			catch(Exception e)
			{
				title = new JLabel(" ");
				this.add(title,BorderLayout.NORTH);
				//e.printStackTrace();
			}
		//	System.out.println(ttl + "first");
			ttl = ClickableLabel.songName;
		//	System.out.println(ttl + "second");
			title = new JLabel(ttl);
			title.setFont(new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 25));
			title.setForeground(new Color(250,250,250));
		
			
			this.add(title,BorderLayout.NORTH);
			revalidate();
		}
		
	}
	
}
