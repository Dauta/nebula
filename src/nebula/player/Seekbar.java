package player;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Seekbar extends JPanel implements MouseListener
{
	
	float duration = 0;
	float progress = 0;
	float sliderX = 0;
	boolean waiting = true;
	Music music;
	String current;
	
	int mouseX = 0;
		
	public Seekbar(Music m)
	{
		music = m;
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
	
		super.paintComponent(g);
		
		if(music.nowPlaying)								//change the slider color :)))
		g.setColor(new Color(255,250,250,170));
		else
		g.setColor(new Color(150,150,255,170));		
		
		g.fillRect(this.getWidth() * 2/100, this.getHeight() * 4/10, this.getWidth() * 96/100, this.getHeight() * 2/10);		//draw seekbar
		
		if(music.nowPlaying)								//change the slider color :)))
			g.setColor(new Color(255,255,255,255));
			else
			g.setColor(new Color(180,180,255,255));	
		//draw slider
		g.fillRect((int) (sliderX + this.getWidth()*15/1000), this.getHeight() * 4/10, this.getWidth() * 1/100, this.getHeight() * 2/10 );  
		
	}
	
	ActionListener sliderListener = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
                	SetDuration();		
		}
	};
	
	public void SetDuration()
	{
		//System.out.println("Current is: " + current);
	//	System.out.println(ClickableLabel.songName);
		//current = music.song;
		
		if(current != ClickableLabel.songName || waiting)
		{
			try
			{
			duration = music.stream.available();	
			waiting = false;
			}
			catch(Exception e)
			{	

			}
		}
		else
		{
			
			try
			{
				progress = music.stream.available();	
				sliderX =  ((duration - progress) / duration) * getWidth() * 96/100;
			
				revalidate();
			}
			catch(Exception e)
			{

			}
					
			if(music.finished)
			{
				sliderX = 0;
				waiting = true;
			}
		}
		
		current = music.song;
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		mouseX = this.getMousePosition().x;
		System.out.println(mouseX);
		
		if(mouseX <= this.getWidth() * 98/100 && mouseX >= this.getWidth() * 15/1000)
		sliderX = mouseX - this.getWidth() * 20/1000;
		
		
		if(music.nowPlaying)
		{
			
			music.Pause();	
			music.audioPaused = (long)duration - ((long) (duration * (sliderX / (this.getWidth() * 96/100))));
			music.Play();
			
		}
		else
		{
			music.audioPaused = (long)duration - ((long) (duration * (sliderX / (this.getWidth() * 96/100))));
		}
		
	
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
