package player;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

//import org.usb4java.*;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

@SuppressWarnings("serial")
public class ClickableLabel extends JPanel implements MouseListener
{
	
	static boolean flag = false;
	static String songName;
	public JLabel artist;
	public JLabel album;
	public JLabel title;
	Playlist pl;
	JPopupMenu pop; 
	public boolean clicked = false;
	static boolean highlight = false;
	static Color baseColor = new Color(255,255,255);
	static Color highColor = new Color(200,240,250);
	String current = null;
	
	
	public ClickableLabel(String artst, String ttle, Playlist p)
	{
		pl = p;
		
	//	this.setLayout(new GridLayout(0,2));
		
		artist = new JLabel();
		title = new JLabel();
		
		artist.setText(artst);
		artist.setFont(new Font("Calibri", Font.LAYOUT_LEFT_TO_RIGHT, 20));
		artist.setForeground(baseColor);
	//	artist.setSize(d)
		//artist.setOpaque(true);
		
		title.setText(ttle);
		title.setFont(new Font("Calibri", Font.CENTER_BASELINE, 20));
		title.setForeground(baseColor);
	
		if(!flag)
		{
			this.setBackground(new Color(80,100,100,100));
		}
		else
		{
			
			this.setBackground(new Color(100,130,130,100));
		}
		
		this.setSize((int)MusicPlayer.masterSize.getWidth() * 720/1000, (int)MusicPlayer.masterSize.getHeight() * 5/100);
		this.setPreferredSize(this.getSize());
		this.setMaximumSize(this.getSize());
		
		this.add(artist);
		this.add(title);
		//this.setSize(1000, this.getHeight());
		//this.setPreferredSize(this.getSize());
		
		flag = !flag;
		this.addMouseListener(this);
		//new Timer(30, this.makeRed()).start();
		
	}
	
	
	public void Pop(MouseEvent e)
	{
		pop = new JPopupMenu();
		
		JMenuItem delete = new JMenuItem("Delete");
		delete.addActionListener(deleteListener);
		pop.add(delete);
		
		JMenuItem copy = new JMenuItem("Send to Phone");
		copy.addActionListener(copyListener);
		pop.add(copy);
		
		pop.show(e.getComponent(), e.getX(), e.getY());
		
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getButton() == MouseEvent.BUTTON3)
		{
			//System.out.println("RightClick");
			Pop(e);
			
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
		if(!ClickableLabel.highlight)
		{
			this.title.setForeground(new Color(200, 240, 250));	
			this.artist.setForeground(new Color(200, 240, 250));
			this.firePropertyChange(getName(), null, clicked);
			ClickableLabel.highlight = true;
			
			this.revalidate();
		}
		else
		{
			for(int i = 0; i < pl.playlistDisplay.getComponentCount(); i++)
			{
				for(int j = 0; j < ((Container) (pl.playlistDisplay.getComponent(i))).getComponentCount(); j++ )
					if(songName != this.title.getText() + " - " + this.artist.getText())
					((Container)(pl.playlistDisplay.getComponent(i))).getComponent(j).setForeground(baseColor);
			}
			this.title.setForeground(new Color(200,240,250));
			this.artist.setForeground(new Color(200,240,250));
			this.firePropertyChange(getName(), null, clicked);
			
			this.revalidate();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getClickCount() % 2 == 0 && !e.isConsumed()) 
		{
		     e.consume();
		     
		    // if(!pl.fileList.isEmpty())
		     songName = this.title.getText() + " - " + this.artist.getText();
		     System.out.println(songName);
		     System.out.println(Playlist.name);
		    
		}
	}
	

	public void deleteFile()
	{
		
	System.out.println("nice");
		
		Container par = this.getParent();
		par.remove(this);
		pl.mainList.get(Playlist.name).remove(this.title.getText() + " - " + this.artist.getText());
		par.revalidate();
	
	}
	
	public void copyFile()
	{
		File source = new File((String)pl.mainList.get(Playlist.name).get(this.title.getText() + " - " + this.artist.getText()));
		File dest = new File("C:\\Users\\irakli\\Desktop\\" + this.title.getText() + " - " + this.artist.getText() + ".mp3");
		
		FileInputStream input;
		FileOutputStream output;
		
		try
		{
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
					byte[] buffer = new byte[4096];
				int byteCount;
			 while ((byteCount = input.read(buffer)) != -1)
				        // Read until EOF
				   output.write(buffer, 0, byteCount); // write
			 
			 input.close();
			 output.close();
				    
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
	}
	
	
	ActionListener copyListener = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e) {
		
			copyFile();	
		}
	};
	
	ActionListener deleteListener = new ActionListener()
	{
		public void actionPerformed(ActionEvent e) {
			
			deleteFile();
		}
	};
	
	public ActionListener isHighlited()
	{
		if(this.highlight)
		{
			this.setForeground(new Color(225,220,255));
			revalidate();
		}
			
		return null;
	}

	
	
	
}
